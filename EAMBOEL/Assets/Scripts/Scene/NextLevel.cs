﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
    public GameObject nextScene;
    public GameObject message;
    static private bool completed1 = false;
    static private bool completed2 = false;

    private IEnumerator coroutine;

    void OnTriggerEnter2D(Collider2D other)
    {
        FindObjectOfType<AudioManager>().Play("Portal");
        Scene actualScene = SceneManager.GetActiveScene();

        if(other.CompareTag("Player"))
        {
            if(actualScene.buildIndex == 1) 
            {
                if(nextScene.name == "World_1")
                    SceneManager.LoadScene(2);
                else if(nextScene.name == "World_2" && completed1)
                    SceneManager.LoadScene(7);
                else if(nextScene.name == "World_3" && completed2)
                    SceneManager.LoadScene(12);
                else
                {
                    coroutine = alertMessage();
                    StartCoroutine(coroutine);
                }
            }
            else if(actualScene.buildIndex == 6 || actualScene.buildIndex == 11)
            {
                if(actualScene.buildIndex == 6)
                    completed1 = true;
                if(actualScene.buildIndex == 11)
                    completed2 = true;
                SceneManager.LoadScene(1);
            }
            else
            {
                SceneManager.LoadScene(actualScene.buildIndex + 1);
            }
        }
    }

    private IEnumerator alertMessage()
    {
        message.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        message.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Return : MonoBehaviour
{

    private IEnumerator coroutine;

    void Start()
    {
        coroutine = load();
        StartCoroutine(coroutine);
    }

    private IEnumerator load()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(1);
    }
}

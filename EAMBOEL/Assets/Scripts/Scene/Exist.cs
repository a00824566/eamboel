﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exist : MonoBehaviour
{

    public static Exist instance;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;  
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
}

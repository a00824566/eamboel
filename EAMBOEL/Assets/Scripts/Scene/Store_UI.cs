﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Store_UI : MonoBehaviour
{
    public GameObject storeUI;
    public GameObject bulletUp;
    public GameObject rayUp;
    public GameObject healthUp;
    public GameObject speedUp;

    public GameObject hero;
    public GameObject bullet;

    static public int bCost = 10;
    static public int rCost = 10;
    static public int hCost = 10;
    static public int sCost = 10;

    public static bool storeActive;

    void Update()
    {
      if(Input.GetKeyDown(KeyCode.Space))
      {
        Debug.Log("bCost: " + bCost);
        Debug.Log("rCost: " + rCost);
        Debug.Log("hCost: " + hCost);
        Debug.Log("sCost: " + sCost);
      }
      if(storeActive)
      {
        showStore();
      }
      else
      {
        quitShop();
      }
    }
    

    void OnTriggerEnter2D(Collider2D other)
    {
      if(other.CompareTag("Player"))
      {
        storeActive = true;
      }
    }

    void showStore()
    {
      storeUI.SetActive(true);
      Time.timeScale = 0f;
      storeActive = true;
    }

    public void quitShop()
    {
      storeUI.SetActive(false);
      Time.timeScale = 1f;
      storeActive = false;
    }

    public void bulletUpgrade()
    {
      if(Economy.money >= bCost)
      {
        FindObjectOfType<AudioManager>().Play("Buy");
        Economy.money -= bCost;
        string cost;
        bCost += 10;
        cost = bCost.ToString();
        bulletUp.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = cost;
        Bullet.damage += 10;
      }
    }

    public void rayUpgrade()
    {
      if(Economy.money >= rCost)
      {
        FindObjectOfType<AudioManager>().Play("Buy");
        Economy.money -= rCost;
        string cost;
        rCost += 10;
        cost = rCost.ToString();
        rayUp.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = cost;
        Player_Shooting.rayDamage += 10;
      }
    }

    public void healthUpgrade()
    {
      if(Economy.money >= hCost)
      {
        FindObjectOfType<AudioManager>().Play("Buy");
        Economy.money -= hCost;
        string cost;
        hCost += 10;
        cost = hCost.ToString();
        healthUp.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = cost;
        Player_Health.maxHealth += 10;
      }
    }

    public void speedUpgrade()
    {
      if(Economy.money >= sCost)
      {
        FindObjectOfType<AudioManager>().Play("Buy");
        Economy.money -= sCost;
        string cost;
        sCost += 10;
        cost = sCost.ToString();
        speedUp.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = cost;
        Player_Movement.speed += 1f;
      }
    }
}

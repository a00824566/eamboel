﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    static public float speed = 5f;

    public Rigidbody2D rb;

    Vector2 movement;
    Vector2 mouse;
    
    public Camera cam;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Speed: " + speed);
        }

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        mouse = cam.ScreenToWorldPoint(Input.mousePosition);
    }

    void FixedUpdate() 
    {
        rb.MovePosition(rb.position + movement * speed * Time.fixedDeltaTime);
        
        Vector2 look = mouse - rb.position;
        float angle = Mathf.Atan2(look.y, look.x) * Mathf.Rad2Deg + 90f;
        rb.rotation = angle;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Shooting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    public int weapon = 1;
    public float bulletForce = 20;
    static public int rayDamage = 20;
    public GameObject hitEffect;
    public LineRenderer lineRenderer;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("RDamage: " + rayDamage);
        }
        if(Input.GetButtonDown("Fire1") && weapon == 1)
        {
            Shoot();
        }
        else if(Input.GetButtonDown("Fire1") && weapon == 2)
        {
            StartCoroutine(Shoot2());
        }

        if(Input.GetKeyDown(KeyCode.Tab))
        {
            if(weapon == 1)
            {
                weapon = 2;
            }
            else
            {
                weapon = 1;
            }
        }
    }

    void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.up * -bulletForce, ForceMode2D.Impulse);
    }

    IEnumerator Shoot2()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(firePoint.position, -firePoint.up);
        if(hitInfo.transform.gameObject.tag == "Enemy")
        {
            hitInfo.transform.gameObject.GetComponent<Enemy_Health>().TakeDamage(rayDamage);
        }
        GameObject effect = Instantiate(hitEffect, hitInfo.point, Quaternion.identity);
        Destroy(effect, 0.5f);

        lineRenderer.SetPosition(0, firePoint.position);
        lineRenderer.SetPosition(1, hitInfo.point);
        FindObjectOfType<AudioManager>().Play("LaserShot");

        lineRenderer.enabled = true;

        yield return new WaitForSeconds(0.02f);

        lineRenderer.enabled = false;    
    }
}

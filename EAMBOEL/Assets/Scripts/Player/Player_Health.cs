﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Health : MonoBehaviour
{
    static public int health = 100;
    static public int maxHealth = 100;
    public GameObject deathEffect;
    public Health_Bar hb;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Health: " + health);
            Debug.Log("Max Health: " + maxHealth);
        }
    }

    public void TakeDamage(int damage)
    {
        FindObjectOfType<AudioManager>().Play("Damage");
        health -= damage;
        hb.setHealth(health);
        
        if(health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        FindObjectOfType<AudioManager>().Play("PlayerDeath");
        GameObject de = Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(de, 0.5f);
        Destroy(gameObject);
        SceneManager.LoadScene(18);
    }
}

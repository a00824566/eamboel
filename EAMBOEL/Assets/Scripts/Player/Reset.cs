﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour
{
    //public Player_Health player;
    public Health_Bar hb;

    void Update()
    {
        hb.setMaxHealth(Player_Health.maxHealth);
        Player_Health.health = Player_Health.maxHealth;
    }
}

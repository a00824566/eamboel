﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Economy : MonoBehaviour
{
    static public int money = 0;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Money: " + money);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Coin"))
        {   
            FindObjectOfType<AudioManager>().Play("Coin");
            WinMoney();
            //ScoreManager.instance.ChangeScore();
            Destroy(other.gameObject);
        }    
    }

    void WinMoney()
    {
        money += 2;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject hitEffect;
    static public int damage = 40;
    public Rigidbody2D rb;

    void Update()
    {
        /*if(Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("BDamage: " + damage);
        }*/
    }

    void OnCollisionEnter2D(Collision2D hitInfo)
    {
        if(hitInfo.gameObject.tag == "Enemy")
        {
            hitInfo.gameObject.GetComponent<Enemy_Health>().TakeDamage(damage);
        }

        GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
        Destroy(effect, 0.5f);

        Destroy(gameObject);     
    }

}

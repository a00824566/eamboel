﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public GameObject scoreText;

    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void Update()
    {
        string t = Economy.money.ToString();
        scoreText.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = t;
    }
}

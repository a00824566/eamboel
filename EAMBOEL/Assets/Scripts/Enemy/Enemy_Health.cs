﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Health : MonoBehaviour
{
    public int health = 100;
    public GameObject deathEffect;
    public GameObject coin;
    public Health_Bar hb;

    public void TakeDamage(int damage)
    {
        health -= damage;
        hb.setHealth(health);
        
        if(health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        FindObjectOfType<AudioManager>().Play("EnemyDeath");
        GameObject de = Instantiate(deathEffect, transform.position, Quaternion.identity);
        GameObject c = Instantiate(coin, transform.position, Quaternion.identity);
        Destroy(de, 0.5f);
        Destroy(gameObject);
    }
}

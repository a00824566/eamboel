﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Attack : MonoBehaviour
{
    public int damage = 20;

    void OnCollisionEnter2D(Collision2D hitInfo) {
        Player_Health player = hitInfo.gameObject.GetComponent<Player_Health>();
        if(player != null)
        {
            player.TakeDamage(damage);
        }
    }
}


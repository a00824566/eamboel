﻿using UnityEngine.Audio;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

    void Awake()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    void Start() 
    {
        Scene actualScene = SceneManager.GetActiveScene();
        int actualIndex = actualScene.buildIndex;

        if(actualIndex == 0 || actualIndex == 1)
        {
            FindObjectOfType<AudioManager>().Play("BaseMusic");
        }
        else if(actualIndex == 6 || actualIndex == 11 || actualIndex == 16 || actualIndex == 21 || actualIndex == 26)
        {
            FindObjectOfType<AudioManager>().Play("MusicBoss");
        }
        else
        {
            FindObjectOfType<AudioManager>().Play("MusicLevel");
        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }
}
